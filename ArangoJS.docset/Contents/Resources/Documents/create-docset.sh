#!/usr/bin/env bash

curl -o docsets.md https://raw.githubusercontent.com/arangodb/arangojs/master/docs/Drivers/JS/Reference/README.md

cat docsets.md | pandoc -f gfm > index.html

dashing build arangojs
