# Build script for Dash docset

## Useful links

[The official ArangoDB JavaScript driver](https://github.com/arangodb/arangojs)

[Online documentation](https://github.com/arangodb/arangojs/blob/master/docs/Drivers/JS/README.md)

[Docset generation guide](https://kapeli.com/docsets)

## Requirements

* [Pandoc](http://pandoc.org)
* [Dashing](https://github.com/technosophos/dashing)

### Installation on OSX

```
brew install pandoc && \
brew install dashing
```

## Usage

From command shell:
```
$ ./generate-docset.sh
```